package es.migra.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import es.migra.bean.CreateGroup;
import es.migra.bean.NameSpaces;
import es.migra.bean.Repo;
import es.migra.service.impl.IRepoServiceImpl;

@Controller
public class RepoController {

	private Logger log = Logger.getLogger(RepoController.class);

	private String PRIVATE_TOKEN = "jr8p-hZCtgUepvBd9XuC";
	private String PRIVATE_ROOT = "root:Ri-5nV5,";
	private String PRIVATE_ROOT_SVN = "admin:admin123";

	@Autowired
	private IRepoServiceImpl sc;

	@RequestMapping(value = " /login", method = RequestMethod.GET)
	public String getLogin(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, ModelMap model) {

		log.info("To Login");

		if (error != null) {
			model.addAttribute("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addAttribute("msg", "You've been logged out successfully.");
		}

		return "login";
	}

	@RequestMapping(value = " /", method = RequestMethod.GET)
	public String getAll(HttpSession ses, ModelMap map) {
		Gson g = new Gson();
		String tJson = g.toJson(sc.All());
		ses.setAttribute("repos", tJson);
		return "index";
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/procesoNew", params = { "reposvn", "areasvn", "repogit", "grupogit",
			"sd" }, method = RequestMethod.GET)
	public @ResponseBody String processNew(@RequestParam(value = "reposvn") String reposvn,
			@RequestParam(value = "areasvn") String areasvn, @RequestParam(value = "grupogit") String grupogit,
			@RequestParam(value = "repogit") String repogit, @RequestParam(value = "sd") String sd, ModelMap map)
					throws Exception {

		log.info("** Migrando New ** " + "reposvn " + reposvn + " " + "arasvn " + areasvn + " " + "repogit " + repogit
				+ " " + "grupogit " + grupogit + " " + "sd " + sd);

		Repo repo = new Repo();

		repo.setAreasvn(areasvn);
		repo.setGrupogit(grupogit);
		repo.setRepogit(repogit);
		repo.setReposvn(reposvn);
		repo.setFecha(new Date());
		repo.setMigrado((byte) 1);
		repo.setSd(sd);

		String cabecera = " http://" + PRIVATE_ROOT_SVN + "@srxmsubversion.mutua.es/svn";

		String httpsvn = cabecera + "/" + repo.getAreasvn() + "/" + repo.getReposvn();

		log.info("MIGRANDO SVN " + httpsvn);

		// consultaGrupo
		int idGrupo = consultaGrupo(grupogit);

		if (idGrupo <= 0)
			// CREAMOS EL GRUPO PARA EL REPOSITORIO
			idGrupo = createGroup(this.PRIVATE_TOKEN, grupogit);

		if (idGrupo > 0) {
			// descargamos el contenido de svn
			downloadSvn(httpsvn);
			// CREAMOS EL GRUPO
			createRepo(this.PRIVATE_TOKEN, repogit, String.valueOf(idGrupo));
			// AÑADIMOS AL REPOSITORIO REMOTO
			remoteAdd(this.PRIVATE_ROOT, repogit, grupogit);
			// APAÑO PARA QUE COJA LOS TRUNK Y BRANCHES
			wrapTrunkBranches();
			// HACEMOS PUSH SOBRE EL REPO REMOTO
			pushGit(repo.getReposvn());

			// ACTUALIZAMOS BBDD
			sc.create(repo);
		} else {
			log.error("No es posible crear repo del SD " + sd);
			throw new Exception("No es posible crear repo del SD " + sd);
		}

		return "";

	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/proceso", params = { "id", "git", "grupo", "sd" }, method = RequestMethod.GET)
	public String processId(@RequestParam(value = "id") int id, @RequestParam(value = "git") String git,
			@RequestParam(value = "grupo") String grupo, @RequestParam(value = "sd") String sd, ModelMap map)
					throws Exception {

		log.info("id " + id);

		Repo repo = new Repo();
		repo.setId(id);
		repo = sc.read(repo);
		String cabecera = " http://" + PRIVATE_ROOT_SVN + "@srxmsubversion.mutua.es/svn";

		String httpsvn = cabecera + "/" + repo.getAreasvn() + "/" + repo.getReposvn();

		log.info("MIGRANDO SVN " + httpsvn);

		// consultaGrupo
		int idGrupo = consultaGrupo(grupo);

		if (idGrupo <= 0)
			// CREAMOS EL GRUPO PARA EL REPOSITORIO
			idGrupo = createGroup(this.PRIVATE_TOKEN, grupo);

		if (idGrupo > 0) {
			// descargamos el contenido de svn
			downloadSvn(httpsvn);
			// CREAMOS EL GRUPO
			createRepo(this.PRIVATE_TOKEN, git, String.valueOf(idGrupo));
			// AÑADIMOS AL REPOSITORIO REMOTO
			remoteAdd(this.PRIVATE_ROOT, git, grupo);
			// APAÑO PARA QUE COJA LOS TRUNK Y BRANCHES
			wrapTrunkBranches();
			// HACEMOS PUSH SOBRE EL REPO REMOTO
			pushGit(repo.getReposvn());

			// ACTUALIZAMOS BBDD
			repo.setFecha(new Date());
			repo.setMigrado((byte) 1);
			repo.setGrupogit(grupo);
			repo.setRepogit(git);
			repo.setSd(sd);
			sc.update(repo);
		} else {
			log.error("No es posible crear repo del SD " + sd);
			throw new Exception("No es posible crear repo del SD " + sd);
		}

		return "index";
	}

	private int consultaGrupo(String grupo) throws Exception {
		// TODO Auto-generated method stub

		Runtime rt = Runtime.getRuntime();

		String[] cmdPut = { "/bin/sh", "-c",
				"curl --header 'PRIVATE-TOKEN: jr8p-hZCtgUepvBd9XuC' 'http://srkmcontdelivery.mutua.es/api/v3/groups'" };

		Process proc = rt.exec(cmdPut);

		InputStream stdin = proc.getInputStream();
		InputStreamReader isr = new InputStreamReader(stdin);
		BufferedReader br = new BufferedReader(isr);
		String line = null;

		// LEEMOS LA SALIDA PARA VER EL ID DEL GRUPO GENERADO HACE FALTA UN POJO

		List<NameSpaces> lNames = new ArrayList<NameSpaces>();
		while ((line = br.readLine()) != null) {

			if (line.contains("id")) {

				//
				Gson g = new Gson();

				JsonArray jsonArray = new JsonParser().parse(line).getAsJsonArray();
				for (int i = 0; i < jsonArray.size(); i++) {
					JsonElement str = jsonArray.get(i);
					NameSpaces obj = g.fromJson(str, NameSpaces.class);
					lNames.add(obj);
				}

				break;
			}

			log.info("    info   " + line);
		}

		boolean encontrado = false;
		int id = 0;
		Iterator<NameSpaces> it = lNames.iterator();
		while (it.hasNext()) {
			NameSpaces ne = it.next();
			if (ne.getPath().equals(grupo)) {
				encontrado = true;
				id = ne.getId().intValue();
				break;
			}
		}

		if (encontrado) {
			log.info("ID Encontrado " + id);
		}

		proc.waitFor();
		proc.destroy();
		rt.runFinalization();

		return id;

	}

	private void pushGit(String repoClean) throws IOException, InterruptedException {
		// TODO Auto-generated method stub

		Runtime rt = Runtime.getRuntime();

		String[] cmdPut = { "/bin/sh", "-c",
				"cd /home/svn/tmp; git push --all origin; git push --tags origin; rm -fr .git; rm -fr *" };

		Process proc = rt.exec(cmdPut);

		printSalidaStd(proc);

		proc.waitFor();
		proc.destroy();
		rt.runFinalization();

	}

	private void wrapTrunkBranches() throws IOException, InterruptedException {

		/*
		 * SE LLAMA AL SH POR QUE SI NO ES UN FOLLON ESCAPAR EL CONTENIDO DEL
		 * FICHERO
		 */

		Runtime rt = Runtime.getRuntime();

		String[] cmdBranches = { "/bin/sh", "-c", "cd /home/svn; ./parser.sh" };

		Process proc = rt.exec(cmdBranches);

		printSalidaStd(proc);

		proc.waitFor();
		proc.destroy();
		rt.runFinalization();

	}

	private void remoteAdd(String PRIVATE_ROOT, String repogit, String grupo) throws IOException, InterruptedException {

		Runtime rt = Runtime.getRuntime();

		String[] cmdRemote = { "/bin/sh", "-c", "cd /home/svn/tmp; git remote add origin http://" + PRIVATE_ROOT
				+ "@srkmcontdelivery.mutua.es/" + grupo + "/" + repogit + ".git " };

		System.out.println(
				"git remote add origin http://*****@srkmcontdelivery.mutua.es/" + grupo + "/" + repogit + ".git ");

		Process proc = rt.exec(cmdRemote);
		printSalidaStd(proc);

		proc.waitFor();
		proc.destroy();
		rt.runFinalization();

	}

	private void createRepo(String PRIVATE, String repogit, String idNameSpace)
			throws IOException, InterruptedException {

		Runtime rt = Runtime.getRuntime();

		String[] cmdApi = { "/bin/sh", "-c",
				"cd /home/svn/tmp; curl --request POST http://srkmcontdelivery.mutua.es/api/v3/projects?private_token="
						+ PRIVATE + " -d name='" + repogit + "' -d namespace_id='" + idNameSpace + "'" };

		System.out.println(
				"scurl --request POST http://srkmcontdelivery.mutua.es/api/v3/projects?private_token=****** -d name='"
						+ repogit + "' -d namespace_id='" + idNameSpace + "'");

		rt = Runtime.getRuntime();
		Process proc = rt.exec(cmdApi);

		printSalidaStd(proc);

		proc.waitFor();
		proc.destroy();
		rt.runFinalization();

	}

	private Integer createGroup(String PRIVATE_TOKEN, String grupo) throws Exception {
		// TODO Auto-generated method stub

		Runtime rt = Runtime.getRuntime();

		String cmdCreateGroup[] = { "/bin/sh", "-c",
				"curl -L -H 'PRIVATE-TOKEN: " + PRIVATE_TOKEN + "' -X POST --data 'name=" + grupo + "&path=" + grupo
						+ "&description=" + grupo + "' http://srkmcontdelivery.mutua.es/api/v3/groups" };

		System.out.println("curl -L -H 'PRIVATE-TOKEN: ****' -X POST --data 'name=" + grupo + "&path=" + grupo
				+ "&description=" + grupo + "' http://srkmcontdelivery.mutua.es/api/v3/groups");

		Process proc = rt.exec(cmdCreateGroup);

		InputStream stdin = proc.getInputStream();
		InputStreamReader isr = new InputStreamReader(stdin);
		BufferedReader br = new BufferedReader(isr);
		String line = null;

		// LEEMOS LA SALIDA PARA VER EL ID DEL GRUPO GENERADO HACE FALTA UN POJO

		int idGrupo = 0;
		while ((line = br.readLine()) != null) {

			if (line.contains("id")) {
				Gson g = new Gson();
				CreateGroup group = g.fromJson(line, CreateGroup.class);
				idGrupo = group.getId();
				System.out.println("GRUPO ENCONTRADO: " + idGrupo);
				break;
			}

			log.info("    info   " + line);
		}

		if (idGrupo <= 0) {
			throw new Exception("No es posible crear el grupo");
		}

		printSalidaStd(proc);

		proc.waitFor();
		proc.destroy();
		rt.runFinalization();

		return idGrupo;

	}

	private void downloadSvn(String httpsvn) throws IOException, InterruptedException {
		//
		Runtime rt = Runtime.getRuntime();
		// // Start a new process: UNIX command
		String cmdSvn2Git[] = { "/bin/sh", "-c",
				"cd /home/svn/tmp; svn2git " + httpsvn + " --trunk trunk --tags tags --branches branches" };
		System.out.println("svn2git -m " + httpsvn + " --trunk trunk --tags tags --branches branches");
		Process proc = rt.exec(cmdSvn2Git);
		printSalidaStd(proc);
		proc.waitFor();
		proc.destroy();
		rt.runFinalization();

	}

	private void printSalidaStd(Process proc) throws IOException {

		InputStream stdin = proc.getInputStream();
		InputStreamReader isr = new InputStreamReader(stdin);
		BufferedReader br = new BufferedReader(isr);

		String line = null;

		while ((line = br.readLine()) != null) {
			System.out.println("    info   " + line);
		}

		// InputStream stderr = proc.getErrorStream();
		// InputStreamReader er = new InputStreamReader(stderr);
		// BufferedReader bre = new BufferedReader(er);
		// String linee = null;
		//
		// while ((linee = bre.readLine()) != null)
		// System.out.println(" error " + linee);

	}
}