package es.migra.controller;

import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;

import es.migra.service.impl.IRepoServiceImpl;

@Controller
public class GraphController {

	private Logger log = Logger.getLogger(GraphController.class);

	@Autowired
	private IRepoServiceImpl sc;

	@RequestMapping(value = " /resumen", method = RequestMethod.GET)
	public String getAll(HttpSession ses, ModelMap map) {

		log.info("!!resumen");

		Gson g = new Gson();
		String tJson = g.toJson(sc.getPercentil());
		ses.setAttribute("percentil", tJson);

		return "resumen";
	}

}