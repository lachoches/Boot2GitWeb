package es.migra.dao;

import java.util.List;

public interface IBase<T> {

	public void create(T t);

	public void delete(T t);

	public T read(T t);

	public List<T> All();

	public void update(T t);

}