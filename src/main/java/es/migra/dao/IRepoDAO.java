package es.migra.dao;

import java.util.List;

import es.migra.bean.Repo;

public interface IRepoDAO extends IBase<Repo> {
	
	public List<String> getListAreasSvn(); 
	
	public List<String[]> getPercentil(); 

}