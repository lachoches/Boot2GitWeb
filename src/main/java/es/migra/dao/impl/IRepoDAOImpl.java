package es.migra.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.migra.bean.Repo;
import es.migra.dao.IRepoDAO;

@Repository
@Transactional
public class IRepoDAOImpl implements IRepoDAO {

	@PersistenceContext
	private EntityManager manager;

	public void create(Repo t) {
		// TODO Auto-generated method stub
		manager.persist(t);
	}

	public void delete(Repo t) {
		// TODO Auto-generated method stub

	}

	public Repo read(Repo t) {
		return manager.find(Repo.class, t.getId());
	}

	public void update(Repo t) {
		manager.merge(t);
	}

	public List<Repo> All() {
		List<Repo> lista = manager.createNativeQuery("SELECT * FROM repos").getResultList();
		return lista;
	}

	public List<String> getListAreasSvn() {
		return manager.createNamedQuery("Repo.findUniqueSvnAreas").getResultList();
	}

	public List<String[]> getPercentil() {
		String sql = "SELECT ma.areasvn AS 'Areas',"
				+ "sum(ma.migrado) AS 'NRM',"
				+ "CONCAT(format(sum(ma.migrado) / count(1) * 100 , 2), ' %') AS `% Migrado`,"
				+ "COUNT(1) AS 'Repositorios',"
				+ "CONCAT(format(COUNT(1) / t.cnt * 100 , 2) , ' %') AS `% total` "
				+ "FROM repos ma CROSS JOIN (SELECT COUNT(1) AS 'cnt' FROM repos) t GROUP BY ma.areasvn";
		return manager.createNativeQuery(sql).getResultList();
	}

}
