package es.migra.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import es.migra.bean.Repo;
import es.migra.dao.IRepoDAO;
import es.migra.service.IRepoService;

@Service
public class IRepoServiceImpl implements IRepoService {

	@Autowired
	private IRepoDAO dao;

	public void create(Repo t) {
		// TODO Auto-generated method stub
		dao.create(t);
		
	}

	public void delete(Repo t) {
		// TODO Auto-generated method stub
		
	}

	public Repo read(Repo t) {
		// TODO Auto-generated method stub
		return dao.read(t);
	}

	public List<Repo> All() {
		// TODO Auto-generated method stub 
		return dao.All();
	}

	public void update(Repo t) {
		// TODO Auto-generated method stub
		dao.update(t);
	}

	public List<String> getListAreasSvn() {
		// TODO Auto-generated method stub
		return dao.getListAreasSvn();
	}

	public List<String[]> getPercentil() {
		// TODO Auto-generated method stub
		return dao.getPercentil();
	}



}