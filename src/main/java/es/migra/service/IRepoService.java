package es.migra.service;

import java.util.List;

import es.migra.bean.Repo;
import es.migra.dao.IBase;

public interface IRepoService extends IBase<Repo> {
	
	public List<String> getListAreasSvn();
	
	public List<String[]> getPercentil(); 

}
