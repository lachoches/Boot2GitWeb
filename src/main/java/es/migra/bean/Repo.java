package es.migra.bean;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the repos database table.
 * 
 */
@Entity
@Table(name="repos")
@NamedQueries({
	@NamedQuery(name="Repo.findAll", query="SELECT r FROM Repo r"),
	@NamedQuery(name="Repo.findUniqueSvnAreas", query="select DISTINCT(c.areasvn) from Repo c")
})


public class Repo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String areasvn;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	private String grupogit;

	private byte migrado;

	private String repogit;

	private String reposvn;

	@Column(name="SD")
	private String sd;

	public Repo() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAreasvn() {
		return this.areasvn;
	}

	public void setAreasvn(String areasvn) {
		this.areasvn = areasvn;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getGrupogit() {
		return this.grupogit;
	}

	public void setGrupogit(String grupogit) {
		this.grupogit = grupogit;
	}

	public byte getMigrado() {
		return this.migrado;
	}

	public void setMigrado(byte migrado) {
		this.migrado = migrado;
	}

	public String getRepogit() {
		return this.repogit;
	}

	public void setRepogit(String repogit) {
		this.repogit = repogit;
	}

	public String getReposvn() {
		return this.reposvn;
	}

	public void setReposvn(String reposvn) {
		this.reposvn = reposvn;
	}

	public String getSd() {
		return this.sd;
	}

	public void setSd(String sd) {
		this.sd = sd;
	}

}