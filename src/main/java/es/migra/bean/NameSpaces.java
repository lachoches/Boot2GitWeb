package es.migra.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NameSpaces {

	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("path")
	@Expose
	private String path;
	@SerializedName("kind")
	@Expose
	private String kind;

	/**
	 *
	 * @return The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 *            The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 *
	 * @return The path
	 */
	public String getPath() {
		return path;
	}

	/**
	 *
	 * @param path
	 *            The path
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 *
	 * @return The kind
	 */
	public String getKind() {
		return kind;
	}

	/**
	 *
	 * @param kind
	 *            The kind
	 */
	public void setKind(String kind) {
		this.kind = kind;
	}

}
