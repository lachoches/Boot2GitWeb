package es.migra.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateGroup {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("name")
@Expose
private String name;
@SerializedName("path")
@Expose
private String path;
@SerializedName("description")
@Expose
private String description;
@SerializedName("avatar_url")
@Expose
private Object avatarUrl;
@SerializedName("web_url")
@Expose
private String webUrl;

/**
*
* @return
* The id
*/
public Integer getId() {
return id;
}

/**
*
* @param id
* The id
*/
public void setId(Integer id) {
this.id = id;
}

/**
*
* @return
* The name
*/
public String getName() {
return name;
}

/**
*
* @param name
* The name
*/
public void setName(String name) {
this.name = name;
}

/**
*
* @return
* The path
*/
public String getPath() {
return path;
}

/**
*
* @param path
* The path
*/
public void setPath(String path) {
this.path = path;
}

/**
*
* @return
* The description
*/
public String getDescription() {
return description;
}

/**
*
* @param description
* The description
*/
public void setDescription(String description) {
this.description = description;
}

/**
*
* @return
* The avatarUrl
*/
public Object getAvatarUrl() {
return avatarUrl;
}

/**
*
* @param avatarUrl
* The avatar_url
*/
public void setAvatarUrl(Object avatarUrl) {
this.avatarUrl = avatarUrl;
}

/**
*
* @return
* The webUrl
*/
public String getWebUrl() {
return webUrl;
}

/**
*
* @param webUrl
* The web_url
*/
public void setWebUrl(String webUrl) {
this.webUrl = webUrl;
}

}
