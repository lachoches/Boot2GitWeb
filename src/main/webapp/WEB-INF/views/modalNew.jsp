<!-- Modal -->
<div class="modal fade" id="nuevoRepo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Nuevo Repo</h4>
			</div>

			<!-- Modal Body -->
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="reposvn">Repo Svn</label> <input type="text" class="form-control" id="reposvn" placeholder="Repositorio svn" />
					</div>
					<div class="form-group">
						<label for="areasvn">Area Svn</label> <input type="text" class="form-control" id="areasvn" placeholder="Area svn" />
					</div>
					<div class="form-group">
						<label for="repogit">Repo Git</label> <input type="text" class="form-control" id="repogit" placeholder="Repo Git" />
					</div>
					<div class="form-group">
						<label for="grupogit">Grupo Git</label> <input type="text" class="form-control" id="grupogit" placeholder="Grupo Git" />
					</div>
					<div class="form-group">
						<label for="sd">C�digo Service Desk</label> <input type="text" class="form-control" id="sd" placeholder="C�digo Service Desk" />
					</div>
                  <div id="state" class="alert alert-info">xxx</div>	
				</form>
			</div>

			<!-- Modal Footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button id="btnMigrar" type="button" class="btn btn-primary">Migrar repo</button>
			</div>
		</div>
		
		
	
	</div>
</div>
<script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>

<script>
	$(document).ready(function() {

		$("#state").hide();
		console.log("Jquery Rocks!");

		$("#btnMigrar").on("click", function() {

			console.log($(this).text());
			
			var reposvn = $("#reposvn").val();
			var areasvn = $("#areasvn").val();
			var repogit = $("#repogit").val();
			var grupogit = $("#grupogit").val();
			var serdesk = $("#sd").val();

			
			
				
			
			if(reposvn == "" || areasvn == "" || repogit == "" || grupogit == "" || serdesk == ""){							   
			    $("#state").removeClass('alert alert-info alert-danger').addClass("alert alert-danger");
			    $("#state").html(" <strong>Error!</strong> Campos no informados! ");
			    $("#state").show();
			}else{
				$("#state").hide();			
				$('#nuevoRepo').modal('hide');	
				 $('#espera').modal('show');
				 console.log("XXX");
					$.ajax({
				        url: '/Svn2GitWeb/procesoNew',
				        type: 'GET',
				        data: { 'reposvn': reposvn, 'areasvn' : areasvn, 'repogit':repogit, 'grupogit':grupogit, 'sd':serdesk} ,
				        contentType: 'application/json; charset=utf-8',
				        success: function (response) {
				        	$("#state").removeClass('alert alert-info alert-danger').addClass("alert alert-success");
						    $("#state").html(" <strong>OK!</strong>  Migraci�n realizada! ");
						    $("#state").show();
						    $('#espera').modal('hide');
						    location.reload(3000);
				        },
				        error: function () {
				        	$("#state").removeClass('alert alert-info alert-danger').addClass("alert alert-danger");
						    $("#state").html(" <strong>Error 500!</strong> Error en servidor! ");
						    $("#state").show();
						    $('#espera').modal('hide');
				        }
				    }); 
					console.log("???");
			}


		});

	});
</script>