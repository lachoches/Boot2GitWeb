
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>

<style>

.toolbar {
    float: left;
}

</style>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<title>Svn2Git</title>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>

		
</head>
<body>

  <c:url value="/j_spring_security_logout" var="logoutUrl" />

	<div id="" class="alert alert-info">
	  <strong>Info!</strong> Migración repositorios Svn a Git.
	  <a href="<c:url value='/resumen' />" >Resumen</a>
	
	<c:if test="${pageContext.request.userPrincipal.name != null}">		
			Usuario : ${pageContext.request.userPrincipal.name} | <a href="javascript:formSubmit()"> Logout</a>		
	</c:if>
	  
	  	<!-- csrt for log out-->
		<form action="${logoutUrl}" method="post" id="logoutForm">
		  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
	</div>
	
	<div id="info" class="alert alert-info">
	  <strong>Info!</strong> Migración repositorios Svn a Git.
	</div>

	<div class="container">
	
		<table id="data" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Id</th>
					<th>Svn</th>
					<th>Area</th>
					<th>Git</th>
					<th>Grupo</th>
					<th>Migrado</th>
					<th>Fecha</th>
					<th>Service Desk</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

	</div>
	
	<%@ include file="modalLoading.jsp" %>
	
	<%@ include file="modalNew.jsp" %>

</body>
<script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
			
			$('#espera').modal('show');
			 $("#info").hide();
			
					var t = $('#data').DataTable({						
						"columnDefs" : [
						{
							"targets" : 8,
							"data" : "download_link",
							"render" : function(id, type, full, meta) {
								return full[5] == 0 ? '<a class="a" id="'+full[0]+'" href="#">Procesar</a>':'Procesado';									
							}
						}
						, {
							"targets" : 5,
							"render" : function(id, type, full, meta) {
								return full[5] == 0 ? 'No' : 'Si';
							}
						}, {
							"targets" : 3,
							"render" : function(id, type, full, meta) {
								return full[5] == 0 ? '<input id="b'+full[0]+'" type="text" name="git" >':full[3];								
							}
						}
						, {
							"targets" : 4,
							"render" : function(id, type, full, meta) {
								return  full[5] == 0 ? '<input id="c'+full[0]+'" type="text" name="grupo" >':full[4];									
							}
						}
						, {
							"targets" : 7,
							"render" : function(id, type, full, meta) {
								return  full[5] == 0 ? '<input id="d'+full[0]+'" type="text" name="sd" >':full[7];									
							}
						}
						]
					});

					var json = ${repos}	;

					var row = t.row;
					
					$.each(json, function(i, obj) {
						row.add([ obj[0], obj[1], obj[2], obj[3], obj[4], obj[5], obj[6], obj[7], '' ]).draw(false);
					});
					
					
					$('#data').removeClass('display').addClass('table table-striped table-bordered');
					$('#espera').modal('hide');
					
					$('<button id="refresh" class="nuevo">Nuevo Registro</button>').appendTo('div.dataTables_filter');
					
					$(document).on('click', '.nuevo', function(e){						
						$('#nuevoRepo').modal('show');					

					});
					
					$(document).on("click",".a", function() {
							var id = $(this).attr('id');
							var b1 = $("#b"+$(this).attr('id')).val();
							var c2 = $("#c"+$(this).attr('id')).val();
							var d3 = $("#d"+$(this).attr('id')).val();							
// 							console.log( b1 + " " + c2 + " " + d3);							
							if(b1 == "" || c2 == "" || d3 == ""){							   
							    $("#info").removeClass('alert alert-info alert-danger').addClass("alert alert-danger");
							    $("#info").html(" <strong>Error!</strong> ID:: ["+ id + "]  Campos no informados! ");
							    $("#info").show();
							}else{
								$("#info").hide();							
								 $('#espera').modal('show');							
									$.ajax({
								        url: '/Svn2GitWeb/proceso',
								        type: 'GET',
								        data: { 'git': b1, 'grupo' : c2, 'id':id, 'sd':d3} ,
								        contentType: 'application/json; charset=utf-8',
								        success: function (response) {
								        	$("#info").removeClass('alert alert-info alert-danger').addClass("alert alert-success");
										    $("#info").html(" <strong>OK!</strong> ID:: ["+ id + "]  Migración realizada! ");
										    $("#info").show();
										    $('#espera').modal('hide');
										    location.reload(3000);
								        },
								        error: function () {
								        	$("#info").removeClass('alert alert-info alert-danger').addClass("alert alert-danger");
										    $("#info").html(" <strong>Error 500!</strong> ID:: ["+ id + "]  Error en servidor! ");
										    $("#info").show();
										    $('#espera').modal('hide');
								        }
								    }); 
							}
					});					
 
				});
		
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>
</html>