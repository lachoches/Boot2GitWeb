
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<title>Svn2Git</title>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
		
</head>
<body>

	<div id="" class="alert alert-info">
		<strong>Info!</strong> RESUMEN Migración repositorios Svn a Git.
		<a href="<c:url value='/' />" >Listado</a>
	</div>

	<div class="container">

		<table id="resumen" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>AREA</th>
					<th>NRM</th>
					<th>% MIGRADO</th>
					<th>TOTAL</th>
					<th>% TOTAL</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>AREA</th>
					<th>NRM</th>
					<th>% MIGRADO</th>
					<th>TOTAL</th>
					<th>% TOTAL</th>
				</tr>
			</tfoot>
		</table>
	</div>
</body>
<script>

	$(document).ready(function() {
		
		var t = $('#resumen').DataTable();
		$('#resumen').removeClass('display').addClass('table table-striped table-bordered');
		var json = ${percentil}	;

		$.each(json, function(i, obj) {
			t.row.add([ obj[0], obj[1], obj[2], obj[3], obj[4] ]).draw(false);
		});
		
	});
</script>
</html>